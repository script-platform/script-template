import send_status.status as status


def handle(input_data, parameters):
    first = int(input_data["first"])
    second = int(input_data["second"])
    summ = first + second
    print(parameters)
    status.update_progress(50)
    return {"summ": summ}
