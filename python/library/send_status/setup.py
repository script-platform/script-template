import setuptools

setuptools.setup(name='send_status',
                 version='0.2',
                 description='Send script status',
                 url='#',
                 author='kiling',
                 install_requires=['jsonschema'],
                 author_email='kiling@bk.ru',
                 packages=setuptools.find_packages(),
                 zip_safe=False)
