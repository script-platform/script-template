def initialization():
    print("Initialization")


def start():
    print("Start")


def update_progress(progress):
    print("Progress: ", progress)


def send_output_data(output_data):
    print(output_data)
