import json
import os.path
from jsonschema import validate
import send_status.status as status

schema_file = "script/schema.default.json"


def run(handler):
    status.initialization()
    # Load
    if not os.path.isfile(schema_file):
        raise ValueError(f'{schema_file} not found')

    with open(schema_file) as json_file:
        script = json.load(json_file)

    input_data = json.loads(os.environ['SCRIPT_INPUT_DATA'])
    parameters = json.loads(os.environ['SCRIPT_PARAMETERS'])

    # Input data validation
    validate(input_data, script["input_schema"])
    validate(parameters, script["parameters_schema"])

    # Start work script
    status.start()
    output_data = handler(input_data, parameters)

    # Output data validation
    validate(output_data, script["output_shema"])

    status.send_output_data(str(json.dumps(output_data)))
