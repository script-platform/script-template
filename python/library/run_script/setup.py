import setuptools

setuptools.setup(name='run_script',
                 version='0.2',
                 description='Send script status',
                 url='#',
                 author='kiling',
                 install_requires=['requests'],
                 author_email='kiling@bk.ru',
                 packages=setuptools.find_packages(),
                 zip_safe=False)
