from run_script.script import run
from script import handler

if __name__ == '__main__':
    run(handler.handle)
